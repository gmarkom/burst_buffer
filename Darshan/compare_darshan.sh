#!/bin/bash

file1=`ls -ltr /project/logs/darshan/*/*/*/$USER*"_id"$1* | tail -n 1 |  awk '{print $9}'`
file2=`ls -ltr /project/logs/darshan/*/*/*/$USER*"_id"$2* | tail -n 1 |  awk '{print $9}'`


darshan-job-summary.pl $file1
darshan-job-summary.pl $file2



file1_pdf=${file1/darshan.gz/pdf}
file2_pdf=${file2/darshan.gz/pdf}

pdf_file1=`echo $file1_pdf | awk -F "/" '{print $8}'`
pdf_file2=`echo $file2_pdf | awk -F "/" '{print $8}'`

pdfjam $pdf_file1 '1' $pdf_file2 '1' --nup 2x1 --landscape --outfile file1.pdf
pdfjam $pdf_file1 '2' $pdf_file2 '2' --nup 2x1 --landscape --outfile file2.pdf
pdfjam $pdf_file1 '3' $pdf_file2 '3' --nup 2x1 --landscape --outfile file3.pdf

gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=compare_darshan_$1_$2.pdf file1.pdf file2.pdf file3.pdf

rm file1.pdf file2.pdf file3.pdf
#echo $pdf_file
evince compare_darshan_$1_$2.pdf &
