#!/bin/bash

if [ $# -eq 0 ]; then

year=`date +"%Y"`
month=`date +"%-m"`
day=`date +"%-d"`


file=`ls -ltr /project/logs/darshan/$year/$month/$day/$USER* | tail -n 1 |  awk '{print $9}'`

else
file=`ls -ltr /project/logs/darshan/*/*/*/$USER*"_id"$1* | tail -n 1 |  awk '{print $9}'`

fi

darshan-job-summary.pl $file


file2=${file/darshan.gz/pdf}
pdf_file=`echo $file2 | awk -F "/" '{print $8}'`

#echo $pdf_file
evince $pdf_file &
