#Connect to ShaheenII with -X or -Y

source /project/k01/markomg/scripts/darshan/load.sh

#Declaring the OSTs (MPI I/O aggregators) on Lustre, examples
lfs setstripe -c X empty_folder   

export MPICH_MPIIO_HINTS= "wrfinput*:striping_factor=64,wrfrst*:striping_factor=144,\
wrfout*:striping_factor=144"

#Declaring the stripe size
lfs setstripe -s X empty_folder  X in bytes

export MPICH_MPIIO_HINTS= "wrfinput*:striping_factor=64,wrfrst*:striping_factor=144:\
striping_unit=4194304,wrfout*:striping_factor=144:\
striping_unit=2097152


#If you did execute an experiment with Darshan on the same day

open_darshan.sh  

#If you know job id

open_darshan.sh job_id

#For using the script compare_darshan you need the package pdfjam already loaded with the load.sh script above
compare_darshan job_id_1 job_id_2

#MPI

* export MPICH_ENV_DISPLAY=1
Displays all settings used by the MPI during execution

* export MPICH_VERSION_DISPLAY=1
Displays MPI version

* export MPICH_MPIIO_HINTS_DISPLAY=1
Displays all the available I/O hints and their values

* export MPICH_MPIIO_AGGREGATOR_PLACEMENT_DISPLAY=1
Display the ranks that are performing aggregation when using MI-I/O collective buffering

* export MPICH_MPIIO_STATS=2
Statistics on the actual read/write operations after collective buffering

* export MPICH_MPIIO_HINTS="..."
Declare I/O hints

# Burst Buffer

#Check the status of DataWarp (DW)
dwstat most

#Check the available DW nodes
dwstat nodes


#Check if anothe users runs on Burst Buffer
scontrol show burst

#Burst buffer declaring MPI I/O aggregators
#**** Required Cray-MPICH v7.4 and later!!!!
export MPICH_MPIIO_HINTS="wrfrst*:cb_nodes=80,wrfout*:cb_nodes=40


#Executing an application from Lustre while you use BB, declare the path of files in MPICH_MPIIO_HINTS
export MPICH_MPIIO_HINTS="$DW_JOB_STRIPED/wrfinput*:cb_nodes=40 $DW_JOB_STRIPED/wrfout*:cb_nodes=40..."